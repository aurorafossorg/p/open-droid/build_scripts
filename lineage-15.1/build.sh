#!/usr/bin/env bash

source build/envsetup.sh
export LC_ALL=C
export USER=root
breakfast $BUILDSCRIPT_DEVICENAME
export USE_CCACHE=1
ccache -M 5G
export CCACHE_COMPRESS=1
export ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx1536m"
croot
brunch $BUILDSCRIPT_DEVICENAME